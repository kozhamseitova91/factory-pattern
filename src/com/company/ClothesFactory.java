package com.company;

public class ClothesFactory {
    public Clothes getClothes (ClothesTypes type) {
        Clothes toReturn = null;
        switch (type) {
            case DenimClothes:
                toReturn = new DenimClothes();
                break;
            case Dresses:
                toReturn = new Dresses();
                break;
            case Outerwear:
                toReturn = new Outerwear();
                break;
            case TShirt:
                toReturn = new TShirt();
                break;
            default:
                throw new IllegalArgumentException("Wrong clothes type:" + type);
        }
        return toReturn;
    }
}
