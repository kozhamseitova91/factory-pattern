package com.company;

public enum ClothesTypes {
    DenimClothes ("denim clothes"),
    Dresses ("dresses"),
    Outerwear ("outewear"),
    TShirt ("t-shirt");

    private String title;

    ClothesTypes(String title) {
        this.title = title;
    }


    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "ClothesTypes{" +
                "title='" + title + '\'' +
                '}';
    }


}
