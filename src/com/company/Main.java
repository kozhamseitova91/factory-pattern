package com.company;

public class Main {

    public static void main(String[] args) {
        ClothesFactory factory = new ClothesFactory();

        Clothes denim = factory.getClothes(ClothesTypes.DenimClothes);
        Clothes tShirt = factory.getClothes(ClothesTypes.TShirt);

        denim.dress();
        tShirt.dress();
    }
}
